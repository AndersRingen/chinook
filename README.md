<!-- ABOUT THE PROJECT -->
## Data Persistence and Access With JDBC
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://gitlab.com/AndersRingen/chinook/-/blob/main/README.md)
[![pipeline-satus](https://gitlab.com/AndersRingen/chinook/badges/main/pipeline.svg)]()

This is the two-part project where Andreas and Anders learn the basics Spring, JDBC, and SQL queries!


### Built With
This is made with Java and PostgreSQL in the Intellij IDE, and PGAdmin as a database helper tool.
* [Java](https://www.java.com/en/)
* [Intellij](https://www.jetbrains.com/idea/)
* [PostgreSQL](https://www.postgresql.org/)
* [PGAdmin](https://www.pgadmin.org/)


<!-- USAGE EXAMPLES -->

# Explaination

### Pt.1: SQL Statements

The first part of the project was about making scripts with SQL statements that makes tables and relationships, as well as inserting, updating, and deleting data. All scripts are displayed in the [this folder](Appendix-A Scripts).

### Pt.2: Reading Data With JDBC 
The second part was about reading data with JDBC. This means that we first made different records or models of what we were going to extract, like Customer, CustomerCountry, CustomerSpender, and CustomerGenre. With the help of a repository for the customer model, we made different methods for the different operations. All methods included a drivermanager to create a connection between us and the database, that later executed our prepared SQL statement to extract the exact the wanted data. The Customer repository extended a CRUD repository, that included basic create, read, update and delete methods that could be used for all records. To test the methods, we used a Applicationrunner to run the code. 

*Soon to be fullstack world champions.*
