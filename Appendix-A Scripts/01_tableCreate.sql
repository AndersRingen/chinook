DROP TABLE IF EXISTS superhero_power;
DROP TABLE IF EXISTS assistant;
DROP TABLE IF EXISTS power;
DROP TABLE IF EXISTS superhero;

CREATE TABLE superhero (
superhero_id SERIAL NOT NULL,
name VARCHAR(20) NOT NULL,
alias VARCHAR(20),
origin VARCHAR(200),
PRIMARY KEY(superhero_id)
);

CREATE TABLE assistant (
assistant_id SERIAL NOT NULL,
Name VARCHAR(20) NOT NULL,
superhero_id INTEGER NOT NULL,
PRIMARY KEY(assistant_id)
);

CREATE TABLE power (
power_id SERIAL NOT NULL,
name VARCHAR(20) NOT NULL,
description VARCHAR(200) NOT NULL,
PRIMARY KEY(power_id)
);




