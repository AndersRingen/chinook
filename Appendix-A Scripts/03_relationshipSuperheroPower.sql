
CREATE TABLE superhero_power (
superhero_id integer NOT NULL,
power_id integer NOT NULL
);

ALTER TABLE superhero_power ADD PRIMARY KEY (superhero_id, power_id);
ALTER TABLE superhero_power ADD FOREIGN KEY (superhero_id) REFERENCES superhero(superhero_id);
ALTER TABLE superhero_power ADD FOREIGN KEY (power_id) REFERENCES power(power_id);