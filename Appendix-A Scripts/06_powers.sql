INSERT INTO power(name,description) VALUES ('Knowledge','Makes your brain capable of cointaining extreme amounts of popculture references');
INSERT INTO power(name,description) VALUES ('Superstrength','Makes you superstrong');
INSERT INTO power(name,description) VALUES ('Superspeed','Makes you run like a lightning');
INSERT INTO power(name,description) VALUES ('Polyglot','Speaks all laguages in the observable universe');
SELECT * FROM power

INSERT INTO superhero_power(superhero_id,power_id) VALUES(2,2);
INSERT INTO superhero_power(superhero_id,power_id) VALUES(1,2);
INSERT INTO superhero_power(superhero_id,power_id) VALUES(1,4);
INSERT INTO superhero_power(superhero_id,power_id) VALUES(3,1);
SELECT * FROM superhero_power