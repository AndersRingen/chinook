package noroff.chinook.repositories.customer;

import noroff.chinook.models.Customer;
import noroff.chinook.models.CustomerGenre;
import noroff.chinook.models.CustomerSpender;
import noroff.chinook.models.CustomerCountry;
import noroff.chinook.repositories.CRUDRepository;

import java.util.List;

public interface CustomerRepository extends CRUDRepository<Customer, Integer> {

    void customerPage(int i, int i1);

    List<CustomerGenre> getMostPopularGenre(int id); // CustomerGenre specific method to get a customers favorite genre

    Customer findByName(String first_name, String last_name);
    CustomerSpender findHighestSpender();

    CustomerCountry findCountryWithMostCustomers(); // CustomerCountry's specific method, which is why it is outside CRUD
}
