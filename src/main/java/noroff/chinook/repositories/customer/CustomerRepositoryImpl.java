package noroff.chinook.repositories.customer;

import noroff.chinook.models.Customer;
import noroff.chinook.models.CustomerGenre;
import noroff.chinook.models.CustomerSpender;
import noroff.chinook.models.CustomerCountry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository{

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(@Value("${spring.datasource.url}") String url,
                                  @Value("${spring.datasource.username}") String username,
                                  @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public void findAll() {
        String sql = "SELECT * FROM customer"; // reads all customers in the table with all its columns
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) { // for every customer in the table, it creates a customer object
                Customer customer = new Customer( // and adds it to an arraylist
                        result.getInt("customer_id"), // that is printed out in the console
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        customers.forEach(System.out::println);
    }

    @Override
    public Customer findById(Integer id) {
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        Customer customer = null; // The sql statement here retrieves the customer that has the same customer_id
        try(Connection conn = DriverManager.getConnection(url, username,password)) { // as the input id
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            if(result.next()) { // Creates the retrieved customer and creates a Customer object out of it
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }


    @Override
    public Customer findByName(String firstName, String lastName) {
        String sql = "SELECT * FROM customer WHERE first_name=? AND last_name=?";
        Customer customer = null; // Retrieves only the customer that matches the input first and last names
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            if (result.next()) {// Creates the retrieved customer and creates a Customer object out of it
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public CustomerSpender findHighestSpender() {
        String sql = "SELECT customer_id,first_name, last_name,SUM(total) AS total_spending\n" + //Selecting fields to be displayed
                "FROM customer JOIN invoice USING (customer_id)\n" + //Joining appropriate tables with matching id
                "GROUP BY customer_id, first_name\n" + //Grouping to only show 1 row for each customer
                "ORDER BY total_spending DESC LIMIT 1"; //Ordering to get the highest value on the top, and setting limit to 1
        CustomerSpender cSpender = null;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            if(result.next()) {
                cSpender = new CustomerSpender( //Creates a new CustomerSpender object and injects values from the query
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getFloat("total_spending"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cSpender;
    }

    @Override
    public void insert(Customer object) {
        String sql = "INSERT INTO customer (first_name, " +
                "last_name, country, postal_code, phone, email)" +
                " VALUES (?,?,?,?,?,?)"; // This SQL statement creates a now row of customer with all the needed inputs
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, object.first_name());
            statement.setString(2, object.last_name());
            statement.setString(3, object.country());
            statement.setString(4, object.postal_code());
            statement.setString(5, object.phone());// These lines populate the question marks on line 115
            statement.setString(6, object.email());// with the properties of the input object
            // Execute statement
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void customerPage(int limit, int offset){
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";//Selects a limited amount of customers
        List<Customer> customers = new ArrayList<>(); //Creating list to be populated with customers
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) { //In whileloop -> Inserting all the customers that is retrieved from the query into the list
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        customers.forEach(System.out::println);
    }

    @Override
    public List<CustomerGenre> getMostPopularGenre(int id) {
        String sql = "SELECT customer_id, genre_id,genre.name as genrename, COUNT(genre_id) as numberoftracks\n" +
                "FROM customer JOIN (invoice JOIN (invoice_line JOIN (track JOIN genre USING(genre_id)) USING(track_id)) USING(invoice_id)) USING(customer_id)\n" +
                "WHERE customer_id=?\n" + // This statement connects the table genre to the track-table, that connects to the invoice-line,
                "GROUP BY genre_id,customer_id, genre_id,genreName\n" + // that connects to the invoice, that connects to the customer
                "ORDER BY numberoftracks DESC LIMIT 2"; // We count and group the result on genre.id to get each genre for a customer
        List<CustomerGenre> genre = new ArrayList<>(); // Last step is to order the result by number og tracks in desc order with 2 as limit
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            int numtrak = 0;
            while(result.next()) {
                if (genre.size()==1){ // If there is a tie between to of the most popular genres
                    if (numtrak == result.getInt("numberoftracks")){
                        genre.add(new CustomerGenre(result.getString("genrename"), result.getInt("numberoftracks")));
                    } // this will add the second genre if it has the same number of tracks
                    break;
                }
                numtrak = result.getInt("numberoftracks");
                genre.add(new CustomerGenre(result.getString("genrename"),result.getInt("numberoftracks")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genre;
    }

    @Override
    public void update(Customer customer) {
        String sql = "UPDATE customer SET first_name=?, last_name=?, " +
                "country=?, postal_code=?, phone=?, email=? WHERE customer_id=?";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.first_name());
            statement.setString(2, customer.last_name());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postal_code());
            statement.setString(5, customer.phone());
            statement.setString(6, customer.email());
            statement.setInt(7, customer.customer_id());
            // Execute statement
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public CustomerCountry findCountryWithMostCustomers() {
        String sql = "SELECT country FROM customer GROUP BY country ORDER BY COUNT(*) DESC LIMIT 1";
        ResultSet result; // In the SQL statement above include only the country column, and then order it
        CustomerCountry country = null; // by the number of the specific country from highest to lower
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            result = statement.executeQuery();
            if (result.next()){
                country = new CustomerCountry(result.getString("country"));
            }
            return country;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return country;
    }
    @Override
    public int delete(Customer object) {
        return 0;
    }

    @Override
    public int deleteById(Integer id) {
        return 0;
    }
}
