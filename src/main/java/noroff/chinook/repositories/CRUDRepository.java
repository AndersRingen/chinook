package noroff.chinook.repositories;

public interface CRUDRepository<T, U> {
    void findAll();
    T findById(U id);

    void insert (T object);
    void update (T object);
    int delete (T object);
    int deleteById (U id);
}
