package noroff.chinook.models;

public record CustomerCountry(String country) { // This object only contains a string
}
