package noroff.chinook.models;

import java.util.Date;

public record Actor(int actor_id, String first_name, String last_name, Date last_update) {
}