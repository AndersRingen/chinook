package noroff.chinook.models;

public record CustomerGenre(String genre, int numberOfTracks) { // This is the CustomerGenre record/model that takes in only a string
}
