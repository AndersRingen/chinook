package noroff.chinook.models;

public record CustomerSpender(int customer_id,
                              String first_name,
                              String last_name,
                              float spending_sum
                              ) {
}
