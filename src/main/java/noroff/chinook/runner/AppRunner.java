package noroff.chinook.runner;

import noroff.chinook.models.Customer;
import noroff.chinook.repositories.customer.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements ApplicationRunner {
    final CustomerRepository customerRepository;
    public AppRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        customerRepository.findAll();
        customerRepository.customerPage(5,2);
        System.out.println(customerRepository.findById(59));
        Customer updateCustomer = new Customer(59,"Andy","","Sverige","","","");
        customerRepository.update(updateCustomer);
        System.out.println(customerRepository.findById(59));
        System.out.println(customerRepository.findHighestSpender());
        System.out.println(customerRepository.findCountryWithMostCustomers());
        System.out.println(customerRepository.getMostPopularGenre(12));
    }
}
